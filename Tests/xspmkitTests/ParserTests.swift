//
//  ParserTests.swift
//  xspmkitTests
//

import XCTest
@testable import xspmkit

class ParserTests: XCTestCase {
    override func setUp() {

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformance() throws {
        let text =
        """
        /* XSPM Manifest */

        - project: "xspmSamples.xcodeproj"

        + "https://gitlab.com/Pyroh/CoreGeometry.git" CoreGeometry >= 2.9.0

        # "-macOS_Sample": CoreGeometry
        # iOS_Sample: CoreGeometry
        """

//        self.measure {
            var t = Tokenizer(text: text)
            t.start()
            t.tokens
//        }
    }

}
