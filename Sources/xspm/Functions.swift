//
//  functions.swift
//  xspm
//

import Foundation
import Rainbow
import xspmkit


/// Wait for the user to press a key.
///
/// - note: Comes from [this post on stackoverflow](https://stackoverflow.com/questions/25551321/xcode-swift-command-line-tool-reads-1-char-from-keyboard-without-echo-or-need-to)
/// - Returns: The value of the pressed key.
private func GetKeyPress () -> Int {
    var key: Int = 0
    let c: cc_t = 0
    let cct = (c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c, c) // Set of 20 Special Characters
    var oldt: termios = termios(c_iflag: 0, c_oflag: 0, c_cflag: 0, c_lflag: 0, c_cc: cct, c_ispeed: 0, c_ospeed: 0)
    
    tcgetattr(STDIN_FILENO, &oldt) // 1473
    var newt = oldt
    newt.c_lflag = 1217  // Reset ICANON and Echo off
    tcsetattr( STDIN_FILENO, TCSANOW, &newt)
    key = Int(getchar())  // works like "getch()"
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt)
    return key
}

func confirmProjectEmbedding() -> Bool {
    var key: Int = 0
    CommonOutput.inform(that: "Embedding dependency project...")
    CommonOutput.inform(that: "Before embedding the dependency project the target project must be closed.")
    CommonOutput.feed()
    repeat {
        CommonOutput.ask(if: "Is the target project closed yet ? [y/n]")
        key = GetKeyPress()
        CommonOutput.feed()
        if key == 110 || key == 78 {
            CommonOutput.ask(if: "Do you want to cancel dependency project embedding ? [y/n]")
            key = GetKeyPress()
            CommonOutput.feed()
            if key == 121 || key == 89 {
                CommonOutput.feed()
                CommonOutput.warn(that: "Project won't be embedded. You can do it manually.")
                return false
            } else {
                CommonOutput.say(that: "Close the target project please.")
            }
        }
    } while(key != 121 && key != 89)
    defer {
        CommonOutput.feed()
        CommonOutput.sayItsOkay(because: "Dependency project embedded.")
    }
    return true
}

func displayDiffResult(of diff: DependencyMappingDiffResult, against mappings: [DependencyMapping]) {
    if !diff.newTargets.isEmpty {
        let count = diff.newTargets.count
        CommonOutput.inform(that: "\(count) new target\(count > 1 ? "s" : "") has been added:")
        diff.newTargets.lazy.compactMap { mappings[$0] }.map { "\t - \($0)" }.forEach(CommonOutput.say)
    }
    if !diff.updatedTargets.isEmpty {
        let count = diff.updatedTargets.count
        CommonOutput.inform(that: "\(count) target\(count > 1 ? "s" : "") has been updated:")
        diff.updatedTargets.forEach { (targetResult) in
            CommonOutput.say(that: " - Target \(targetResult.name):")
            if !targetResult.newProducts.isEmpty {
                let products = targetResult.newProducts.joined(separator: ", ")
                CommonOutput.say(that: "\t • \(products) should be added.")
            }
            if !targetResult.obsoleteProduts.isEmpty {
                let products = targetResult.obsoleteProduts.joined(separator: ", ")
                CommonOutput.say(that: "\t • \(products) can be removed.")
            }
            if !targetResult.unchangedProducts.isEmpty {
                let products = targetResult.unchangedProducts.joined(separator: ", ")
                CommonOutput.say(that: "\t • \(products) should remain in place.")
            }
            if !targetResult.removedProducts.isEmpty {
                let products = targetResult.removedProducts.joined(separator: ", ")
                CommonOutput.say(that: "\t • \(products) should be removed.")
            }
        }
    }
    if !diff.removedTargets.isEmpty {
        let count = diff.removedTargets.count
        CommonOutput.inform(that: "\(count) target\(count > 1 ? "s" : "") has been removed:")
        diff.removedTargets.lazy.compactMap { $0 }.map { "\t - \($0)" }.forEach(CommonOutput.say)
    }
    if !diff.unchangedTargets.isEmpty {
        let count = diff.unchangedTargets.count
        CommonOutput.inform(that: "\(count) target\(count > 1 ? "s" : "") has been left unchanged:")
        diff.unchangedTargets.lazy.compactMap { mappings[$0] }.map { "\t - \($0)" }.forEach(CommonOutput.say)
    }
}
