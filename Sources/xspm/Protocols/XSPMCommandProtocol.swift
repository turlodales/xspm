//
//  XSPMCommandProtocol.swift
//  xspm
//

import Foundation
import Commandant
import Result
import xspmkit

typealias XSPMResult = Result<Void, XSPMError>

protocol XSPMCommandProtocol: CommandProtocol {
    func run(_ options: Options) throws
}

extension XSPMCommandProtocol {
    func run(_ options: Options) -> XSPMResult {
        if let verboseOptions = options as? VerboseOptionsProtocol { verboseOptions.adaptVerbosity() }
        if let quietOptions = options as? QuietOptionsProtocol { quietOptions.adaptQuietness() }
        do {
            try self.run(options)
            return .success(())
        } catch let error as XSPMError {
            return .failure(error)
        } catch {
            return .failure(.unknown(error.localizedDescription))
        }
    }
}
