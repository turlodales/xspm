#if os(Linux)
    print("xspm cannot run on Linux...")
    exit(EXIT_FAILURE)
#endif

import Foundation
import Commandant
import Rainbow
import xspmkit

LoggerManager.set(logger: CommonOutput.loggerProxy)
LoggerManager.set(logLevel: .message)

if #available(OSX 10.13, *) {
    xspm.run()
} else {
    CommonOutput.sayItsNotOkay(because: "xspm requires macOS 10.13 or newer.")
}



