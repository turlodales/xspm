//
//  Init.swift
//  xspm
//

import Foundation
import Commandant
import Curry
import Result
import xspmkit
import xcodeproj
import PathKit

struct InitCommandOptions: OptionsProtocol, VerboseOptionsProtocol, QuietOptionsProtocol, SaveLogsOptionsProtocol {
    typealias ClientError = XSPMError
    
    let ignore: Bool
    let force: Bool
    let verbose: Bool
    let quiet: Bool
    let saveLogs: Bool
    
    static func evaluate(_ m: CommandMode) -> Result<InitCommandOptions, CommandantError<XSPMError>> {
        return curry(self.init)
            <*> m <| Switch(flag: "g", key: "no-ignore", usage: "Don't add dependencies folder to gitignore")
            <*> m <| Switch(flag: "f", key: "force", usage: "Reinit dependencies with the same name if any")
            <*> m <| verboseSwitch
            <*> m <| quietSwitch
            <*> m <| saveLogsSwitch
    }
}

struct InitCommand: XSPMCommandProtocol {
    typealias Options = InitCommandOptions
    
    var verb: String = "init"
    var function: String = "🚀 - Initialize dependencies"
    
    func run(_ options: InitCommandOptions) throws {
        let root = Path.current
        
        CommonOutput.say(that: "\n🚀 Initializing dependencies...\n".bold)
        
        let manifest = try Manifest.read(inRoot: root)
        
        try manifest.assert(is: .unresolved)
        
        let package = try self.createPackageFile(inRoot: root, from: manifest, withOptions: options)
        let saveLogs = options.saveLogs
        
        CommonOutput.inform(that: "Creating Package.swift...")
        try package.`init`(saveLog: saveLogs)
        CommonOutput.sayItsOkay(because: "Package.swift created.")
        
        CommonOutput.inform(that: "Resolving dependencies...")
        CommonOutput.feed()
        try package.resolve(saveLog: saveLogs)
        CommonOutput.sayItsOkay(because: "Dependencies resolved.")
        
        CommonOutput.inform(that: "Creating dependency project...")
        try package.generateXcodeproj(saveLog: saveLogs)
        CommonOutput.sayItsOkay(because: "Dependency project created.")
        
        try manifest.embedDependencyProjectIfNeeded(confirm: confirmProjectEmbedding)
        
        if !options.ignore {
            CommonOutput.inform(that: "Adding dependency folder to .gitignore")
            try self.addDependenciesToGitignore(inRoot: root, dependenciesName: manifest.dependencyFolderName)
            CommonOutput.sayItsOkay(because: "Dependency folder added to .gitignore.")
        }
        
        CommonOutput.feed()
        
        CommonOutput.inform(that: "Resolving targets dependendencies...")
        let tree = try package.getDependenciesTree(saveLog: saveLogs)
        let result = try manifest.resolve(against: tree)
        CommonOutput.sayItsOkay(because: "Target dependencies resolved.")
        
        if !result.isUseless {
            CommonOutput.inform(that: "Here is the recommanded project configuration:")
            displayDiffResult(of: result, against: try manifest.getResolvedDependenciesMapping())
        }
        
        CommonOutput.sayJobsDone()
    }
    
    private func createPackageFile(inRoot root: Path, from manifest: Manifest, withOptions options: Options) throws -> Package {
        let projectName = manifest.targetProjectName
        let projectPath = root + projectName
        
        guard projectPath.exists else { throw XSPMError.noSuchProject(projectName) }
        guard !manifest.dependencies.isEmpty else { throw XSPMError.noDependencies }
        
        let package = Package(root: root, manifest: manifest)
        try package.prepareWorkFolder(force: options.force)
        
        return package
    }
    
    private func addDependenciesToGitignore(inRoot root: Path, dependenciesName name: String) throws {
        let gitignorePath = root + ".gitignore"
        var gitignorelines: [String]
        if !gitignorePath.exists {
            CommonOutput.debug(that: "No .gitignore file to update. We will create it.")
            gitignorelines = []
        } else {
            guard let gitignore: String = try? gitignorePath.read() else { return }
            gitignorelines = gitignore.split(separator: "\n").map(String.init)
        }
        
        ["/\(name)", Manifest.resolvedFileName].forEach {
            if gitignorelines.contains($0) {
                CommonOutput.sayItsOkay(because: "\($0) already present.")
            } else {
                gitignorelines.append($0)
            }
        }
        
        try gitignorePath.write(gitignorelines.joined(separator: "\n") + "\n")
    }
}
