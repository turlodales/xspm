//
//  xspm.swift
//  xspm
//

import Foundation
import Commandant
import Result
import xspmkit

struct xspm {
    private init() {} 
    
    static func run() -> Never {
        let registry = CommandRegistry<XSPMError>()
        .register(BootstrapCommand())
        .register(InitCommand())
        .register(UpdateCommand())
        .register(BlankLine())
        .register(ShowCommand())
        .register(StatusCommand())
        .register(CleanCommand())
        .register(BlankLine())
        .register(VersionCommand())
        registry.register(HelpCommand(registry: registry, function: "🤔 - Display general or command-specific help"))
        
        registry.main(defaultVerb: "help") { (err) in
            handleError(error: err)
        }
    }
    
    static func handleError(error: XSPMError) {
        switch error {
        case.manifestAlreadyExists:
            CommonOutput.sayItsNotOkay(because: "The manifest file already exists.")
        case .noProject:
            CommonOutput.sayItsNotOkay(because: "No project found.")
        case .noSuchProject(let projectName):
            CommonOutput.sayItsNotOkay(because: "No such project: \(projectName)")
        case .projectNameIndecidable:
            CommonOutput.sayItsNotOkay(because: "Too many projects cannot magically guess project name.")
        case .unknown(let description):
            CommonOutput.sayItsNotOkay(because: description)
            CommonOutput.sayItsNotOkay()
        case .invalidDependenciesFolderName:
            CommonOutput.sayItsNotOkay(because: "The name for the dependencies folder is not valid.")
        case .noManifest:
            CommonOutput.sayItsNotOkay(because: "Cannot find \(Manifest.manifestFileName) file.")
        case .noDependencies:
            CommonOutput.inform(that: "The manifest file doesn't declare any dependency.")
            CommonOutput.inform(that: "No dependency project will be created.")
            CommonOutput.sayJobsDone()
        case .swiftPMError(let logName):
            CommonOutput.sayItsNotOkay(because: "An spm error occured. Error log has been written to \(logName)")
        case .unresolvedManifest:
            CommonOutput.sayItsNotOkay(because: "It appears that the dependencies are not initialized correctly. Run 'xpsm init -f' and address any issue before retrying.")
        case .ambiguousState:
            CommonOutput.sayItsNotOkay(because: "This xspm integration is damaged. Run 'xspm clean -k && xspm init'")
        case .cannotReadResolved:
            CommonOutput.sayItsNotOkay(because: "Enable to read resolved manifest file.")
        case .invalidManifest:
            CommonOutput.sayItsNotOkay(because: "Manifest file syntax error.")
        case .alreadyInitiated:
            CommonOutput.sayItsNotOkay(because: "The xspm integration has already been initialized. Use 'xspm update' instead.")
        default:
            CommonOutput.sayItsNotOkay()
        }
    }
}
