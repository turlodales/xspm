//
//  Project.swift
//  xspmkit
//

import Foundation
import PathKit
import xcodeproj

public class Project {
    public enum BuildConfiguration {
        case debug, release
        case custom(name: String)
        
        public static var common: [BuildConfiguration] {
            return [.debug, .release]
        }
    }
    
    public private(set) var projectPath: Path
    public private(set) var isDirty: Bool = false
    
    public let project: XcodeProj
    
    public var pbxproj: PBXProj {
        return self.project.pbxproj
    }
    public var sharedData: XCSharedData? {
        return self.project.sharedData
    }
    public var workspace: XCWorkspace {
        return self.project.workspace
    }
    public var projectName: String {
        return self.projectPath.lastComponentWithoutExtension
    }
    
    public var subProjectPaths: [Path] {
        guard let projects = self.rootProject?.projects else { return [] }
        return projects.compactMap {
            $0["ProjectRef"]?.path
        }.map {
            self.projectRoot + $0
        }
    }
    public var nativeTargetNames: [String] {
        return self.pbxproj.nativeTargets.map { $0.name }
    }
    
//    private var defaultLogLevel: LogLevel
    
    private var rootGroup: PBXGroup? {
        do {
            return try self.pbxproj.rootGroup()
        } catch {
            return nil
        }
    }
    private var rootProject: PBXProject? {
        do {
            return try self.pbxproj.rootProject()
        } catch {
            return nil
        }
    }
    private var projectRoot: Path {
        return self.projectPath.parent()
    }
    
    public init(projectPath: Path) throws {
        self.projectPath = projectPath
        self.project = try XcodeProj(path: projectPath)
    }
    
    public func add(subproject: Project) throws {
        guard !self.subProjectPaths.contains(subproject.projectPath) else {
            LoggerManager.logDebug("\(subproject.projectName) is already a sub-project of \(self.projectName)")
            return
        }
        guard let rootGroup = self.rootGroup,
            let rootProject = self.rootProject,
            let embeddedProject = subproject.project.pbxproj.rootObject else {
            LoggerManager.logError("Big error.") // FIXME: Something more expressive may be a nice thing
            return
        }
        let projRef = try rootGroup.addFile(at: subproject.projectPath, sourceRoot: self.projectRoot)
        self.pbxproj.add(object: embeddedProject)
        LoggerManager.logDebug("Subproject loaded.")
        
        LoggerManager.logDebug("Creating group.")
        let productGroup = PBXGroup(children: [], sourceTree: .group, name: "Products", path: nil, includeInIndex: nil, wrapsLines: nil, usesTabs: nil, indentWidth: nil, tabWidth: nil)
        self.pbxproj.add(object: productGroup)
        LoggerManager.logDebug("Creating project reference. ")
        rootProject.projects.append(["ProductGroup": productGroup, "ProjectRef": projRef])
        
        LoggerManager.logInfo("Added project \(subproject.projectName) to \(self.projectName)")
        self.isDirty = true
    }
    
    func set(buildSetting: String, ofTarget target: String, to value: Any, forConfigurations configurations: [BuildConfiguration]) {
        self.pbxproj.targets(named: target).forEach { (target) in
            configurations.forEach({ (configuration) in
                let configurationName: String
                switch configuration {
                case .debug:
                    configurationName = "Debug"
                case .release:
                    configurationName = "Release"
                case .custom(name: let name):
                    configurationName = name
                }
                if let configurationObject =  target.buildConfigurationList?.configuration(name: configurationName) {
                    configurationObject.buildSettings[buildSetting] = value
                    self.isDirty = true
                }
            })
        }
    }
    
    public func contains(subproject: Project) -> Bool {
        return self.subProjectPaths.contains(subproject.projectPath)
    }
    
    public func save(verbosity: LogLevel? = nil) throws {
        if self.isDirty {
            try self.project.write(path: self.projectPath, override: true)
        }
    }
}

extension Project: Equatable {
    public static func ==(lhs: Project, rhs: Project) -> Bool {
        return !lhs.isDirty && !lhs.isDirty && lhs.projectPath == rhs.projectPath
    }
}
