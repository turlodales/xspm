//
//  ResolvedManifest.swift
//  xspmkit
//

import Foundation
import PathKit

public class ResolvedManifest: Codable, Equatable {
//    let timestamp: TimeInterval
    let content: ManifestContent
    let actualDependencyMapping: [DependencyMapping]
    let dependenciesTree: ResolvedDependenciesTree
    
    init(from manifest: Manifest, dependenciesTree tree: ResolvedDependenciesTree) {
        self.content = manifest.content
        self.actualDependencyMapping = manifest.actualDependencyMapping ?? []
        self.dependenciesTree = tree
    }
    
    func write(to path: Path) throws {
        try path.write(try JSONEncoder().encode(self))
    }
    
    public static func ==(lhs: ResolvedManifest, rhs: ResolvedManifest) -> Bool {
        return lhs.content == rhs.content && lhs.actualDependencyMapping == rhs.actualDependencyMapping
    }
}
