//
//  Package.swift
//  xspmkit
//

import Foundation
import PathKit

public class Package {
    public let root: Path
    public var workPath: Path {
        return root + Path(self.dependencyFolderName)
    }
    public var dependencyFolderName: String {
        return self.manifest.dependencyFolderName
    }
    public var packagePath: Path {
        return self.workPath + Path("Package.swift")
    }
    public var targetProjectName: String {
        return self.manifest.targetProjectName
    }
    public var targetProjectPath: Path {
        return self.workPath + Path(self.targetProjectName)
    }
    public var dependencyProjectName: String {
        return "\(self.dependencyFolderName).xcodeproj"
    }
    public var dependencyProjectPath: Path {
        return self.workPath + self.dependencyProjectName
    }
    public var buildPath: Path {
        return self.workPath + ".build"
    }
    
    private var targetName: String { return "\(self.dependencyFolderName)Target" }
    private var dependencies: [Dependency] {
        return self.manifest.dependencies
    }
    private let manifest: Manifest
    private var dependencyTree: ResolvedDependenciesTree?
    
    public init(root: Path, manifest: Manifest) {
        self.root = root
        self.manifest = manifest
    }
    
    public func write() throws {
        let content = self.generatePackageContent()
        
        try self.packagePath.write(content)
        try self.toolsVersion(self.manifest.content.toolsVersion)
    }
    
    public func `init`(saveLog flag: Bool) throws {
        LoggerManager.logDebug(" $ swift package init")
        try SPM.perform(command: .`init`, printOutput: false, saveSuccessLog: flag, extraArgs: ["--type", "library"], inRoot: self.workPath)
        try self.write()
    }
    
    public func resolve(saveLog flag: Bool) throws {
        LoggerManager.logDebug(" $ swift package resolve")
        try SPM.perform(command: .resolve, printOutput: true, saveSuccessLog: flag, inRoot: self.workPath)
    }
    
    public func update(saveLog flag: Bool) throws {
        LoggerManager.logDebug(" $ swift package update")
        try SPM.perform(command: .update, printOutput: true, saveSuccessLog: flag, inRoot: self.workPath)
    }
    
    public func generateXcodeproj(saveLog flag: Bool, fixVersions: Bool = true) throws {
        let consolidatedXcconfigName = ".consolidated_xcconfig.xcconfig"
        let consolidatedXcconfigPath = self.workPath + consolidatedXcconfigName
        var consolidatedXcconfig = ""
        
        if let xcconfigName = self.manifest.content.xcconfig {
            let xcconfigPath = self.root + xcconfigName
            let xcconfig: String = try xcconfigPath.read().trimmingCharacters(in: .whitespacesAndNewlines)
            consolidatedXcconfig.appendLine(xcconfig)
        }
        
        LoggerManager.logDebug("Writing .consolidated_xcconfig.xcconfig...")
        try consolidatedXcconfigPath.write(consolidatedXcconfig)
        
        LoggerManager.logDebug(" $ swift package generate-xcodeproj --xcconfig-overrides \(consolidatedXcconfigName)")
        try SPM.perform(command: .generateXcodeproj, printOutput: false, saveSuccessLog: flag, extraArgs: ["--xcconfig-overrides", "\(consolidatedXcconfigName)"], inRoot: self.workPath)
        
        if (fixVersions) { try self.fixProjectsVersion() }
    }
    
    public func fixProjectsVersion() throws {
        LoggerManager.logDebug("Starting fixing products version...")
        let tree = try self.getDependenciesTree(saveLog: false)
        let dependencyProject = try Project(projectPath: self.dependencyProjectPath)
        LoggerManager.logDebug("\(self.dependencyProjectName) opened.")
        
        dependencyProject.nativeTargetNames.forEach {
            guard let package = tree.package(for: $0) else { return }
            let version = tree.version(for: package)
            LoggerManager.logDebug("Setting \($0) to version \(version)...")
            dependencyProject.set(buildSetting: "CURRENT_PROJECT_VERSION", ofTarget: $0, to: version, forConfigurations: Project.BuildConfiguration.common)
        }
        
        LoggerManager.logDebug("Saving back \(self.dependencyProjectName)...")
        try dependencyProject.save()
        LoggerManager.logDebug("Products version fixed.")
    }
    
    public func toolsVersion(_ version: String? = nil) throws {
        if let version = version {
            LoggerManager.logDebug(" $ swift package tools-version --set \(version)")
            try SPM.perform(command: .toolsVersion, printOutput: false, saveSuccessLog: false, extraArgs: ["--set", version], inRoot: self.workPath)
        } else {
            LoggerManager.logDebug(" $ swift package tools-version --set-current")
            try SPM.perform(command: .toolsVersion, printOutput: false, saveSuccessLog: false, extraArgs: ["--set-current"], inRoot: self.workPath)
        }
    }
    
    public func getDependenciesTree(saveLog flag: Bool) throws -> ResolvedDependenciesTree {
        if let tree = self.dependencyTree {
            return tree
        } else {
            let data = try SPM.perform(command: .showDependencies, printOutput: false, saveSuccessLog: flag, extraArgs: ["--format", "json"], inRoot: self.workPath)
            
            guard let str = String(data: data, encoding: .utf8),
                let startIndex = str.firstIndex(of: "{"),
                let endIndex = str.lastIndex(of: "}"),
                let sanitizedData = String(str[startIndex...endIndex]).data(using: .utf8),
                let tree = try? ResolvedDependenciesTree(fromTreeData: sanitizedData, buildPath: self.buildPath) else {
                    throw XSPMError.cannotGetDependenciesTree
            }
            
            self.dependencyTree = tree
            return tree
        }
    }
    
    private func generatePackageContent() -> String {
        func quoted(_ str: String) -> String { return str.quoted }
        
        var buffer = """
        /*
         * ┌────────────────────────────┐
         * │                            │
         * │     Genereated by xspm     │
         * │                            │
         * └────────────────────────────┘
         */
        
        // Do not modify this file directly. Modify the manifest file instead and use 'xspm update'.
        
        import PackageDescription
        
        let package = Package(
        """
        buffer.appendLine("name: \(self.dependencyFolderName.quoted),", indentLevel: 1)
        
        buffer.appendLine("products: [],", indentLevel: 1)
        
        buffer.appendLine("dependencies: [", indentLevel: 1)
        dependencies.map { "\($0.serialized)," }.forEach { buffer.appendLine($0, indentLevel: 2) }
        
        buffer.appendLine("],", indentLevel: 1)
        buffer.appendLine("targets: [", indentLevel: 1)
        buffer.appendLine(".target(", indentLevel: 2)
        buffer.appendLine("name: \"\(self.dependencyFolderName)\",", indentLevel: 3)

        let deps = self.dependencies.map{ $0.products.map { $0.quoted }.joined(separator: ", ") }.joined(separator: ", ")
        
        buffer.appendLine("dependencies: [\(deps)]),", indentLevel: 3)
        buffer.appendLine("]", indentLevel: 1)
        buffer.appendLine(")")
        
        return buffer
    }
    
    public func prepareWorkFolder(force: Bool) throws {
        if self.workPath.exists {
            if force {
                try self.workPath.delete()
            } else {
                return
            }
        }
        try self.workPath.mkdir()
    }
}
