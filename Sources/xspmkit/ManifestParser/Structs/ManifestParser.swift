//
//  ManifestParser.swift
//  xspmkit
//

import Foundation
import Result

public struct ManifestParser {
    private let manifestString: String
    
    public init(manifest: String) {
        self.manifestString = manifest
    }
    
    public func validateString() -> Result<Bool, ParserError> {
        return self.createParser().validate()
    }
    
    func parseManifestContent() throws -> ManifestContent {
        return try self.createParser().parse()
    }
    
    private func createParser() -> Parser {
        var tokenizer = Tokenizer(text: self.manifestString)
        tokenizer.start()
        
        return Parser(tokens: tokenizer.tokens)
    }
}
