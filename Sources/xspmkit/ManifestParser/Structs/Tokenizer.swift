//
//  Tokenizer.swift
//  xspmkit
//

import Foundation
import ConvenientCharacterSets

struct Tokenizer {
    private(set) var tokens: [Token]
    
    private let streamer: Streamer
    private var tokenData: TokenData
    private var finished: Bool = false
    
    private let kind = CharacterSet(charactersIn: "+-#")
    private let digits = CharacterSet(charactersIn: "0123456789")
    private let numericSign = CharacterSet(charactersIn: "+-")
    private let op = CharacterSet(charactersIn: "~=>")
    private let illegalInSemver = CharacterSet(charactersIn: ",[]{}/\\")
    private let hex = CharacterSet.hexaDecimal
    private let identifierNameStart = CharacterSet.alphanumerics + "_"
    private let identifierName = CharacterSet.alphanumerics + "!@#$%&*()_-+=.?><"
    
    private var previous: Character {
        return self.streamer.previous
    }
    private var current: Character {
        return self.streamer.current
    }
    private var isEOF: Bool {
        return self.streamer.isEOF
    }
    private var isEOL: Bool {
        return self.streamer.isEOL
    }
    
    init(text: String) {
        self.streamer = .init(text: text)
        self.tokens = []
        self.tokenData = .initial()
    }
    
    /// Start tokenizing the current `Streamer` object.
    mutating func start() {
        guard !self.finished else { fatalError("Tokenization has alread been done.") }
        while !self.streamer.isEOF {
            switch self.current {
            case self.current where self.isEOL:
                self.tokens.append(.eol)
                self.streamer.consume()
            case CharacterSet.whitespaces:
                _ = self.streamer.stripWhiteSpace()
                self.tokens.append(.whiteSpace)
            case self.kind:
                self.openNewTokenData()
                self.assumeKind()
            case "\"":
                self.openNewTokenData()
                self.assumeString()
            case "/":
                self.openNewTokenData()
                self.assumeComment()
            case CharacterSet.decimalDigits:
                self.openNewTokenData()
                self.assumeNumeric(signed: false)
            case self.identifierNameStart:
                self.openNewTokenData()
                self.assumeIdentifier()
            case self.op:
                self.openNewTokenData()
                self.assumeOperator()
            case ":":
                self.openNewTokenData()
                self.advance()
                self.closeTokenData()
                self.register(token: .colon(self.tokenData))
            case "[":
                self.openNewTokenData()
                self.advance()
                self.closeTokenData()
                self.register(token: .openbracket(self.tokenData))
            case "]":
                self.openNewTokenData()
                self.advance()
                self.closeTokenData()
                self.register(token: .closebracket(self.tokenData))
            case ",":
                self.openNewTokenData()
                self.advance()
                self.closeTokenData()
                self.register(token: .comma(self.tokenData))
            case "<":
                self.openNewTokenData()
                self.assumeRevision()
            default:
                self.openNewTokenData()
                self.assumeErrorneous()
            }
        }
        self.tokens.append(.eof)
        self.finished = true
    }
    
    
    /// Create a new `TokenData` object.
    /// - precondition: The current `TokenData` object is closed.
    private mutating func openNewTokenData() {
        guard self.tokenData.isClosed else {
            fatalError("You must close the current token data prior opening a new one.")
        }
        self.tokenData = TokenData(start: self.streamer.location)
    }
    
    
    /// Close the current `TokenData` object.
    private mutating func closeTokenData() {
        self.tokenData.close(at: self.streamer.location)
    }
    
    
    /// Store the current character in the buffer and advance the streamer.
    private mutating func advance() {
        guard !self.isEOF else { return }
        self.tokenData.append(self.current)
        self.streamer.consume()
    }
    
    /// Discard current character and advance the streamer.
    private mutating func discard() {
        guard !self.isEOF else { return }
        self.streamer.consume()
    }
    
    
    /// Append the given token to the token list.
    ///
    /// - Parameter token: The token to append.
    /// - precondition: The current `TokenData` object is closed.
    private mutating func register(token: Token) {
        guard self.tokenData.isClosed else {
            fatalError("You must close the current token data prior opening a new one.")
        }
        self.tokens.append(token)
    }
    
    /// Treat the forecoming caracters as being part of a *kind* token.
    private mutating func assumeKind() {
        let canBeNumeric = self.current ?= self.numericSign
        self.advance()
        if canBeNumeric, self.current ?= self.digits {
            self.assumeNumeric(signed: true)
            return
        }
        if !self.isEOF && self.current ?!= .whitespacesAndNewlines {
            self.assumeErrorneous()
            return
        }
        self.closeTokenData()
        self.register(token: .kind(self.tokenData))
    }
    
    /// Treat the forecoming caracters as being part of a *string* token.
    private mutating func assumeString() {
        let delimiter = self.current
        
        repeat {
            self.advance() }
        while !self.isEOF && self.current ?!= .newlines && self.current != delimiter
        
        if self.current == delimiter {
            self.advance()
            self.closeTokenData()
            self.register(token: .string(self.tokenData))
        } else {
            self.assumeErrorneous()
        }
    }
    
    /// Treat the forecoming caracters as being part of a *comment* token.
    private mutating func assumeComment() {
        self.advance()
        if self.current == "/" {
            repeat {
                self.advance()
            } while !self.isEOF && self.current ?!= .newlines
            self.closeTokenData()
        } else if self.current == "*" {
            self.assumeFencedComment()
        } else {
            self.assumeErrorneous()
        }
        
    }
    
    /// Treat the forecoming caracters as being part of a *fenced comment* token.
    private mutating func assumeFencedComment() {
        var fenced = false
        var couldEnd = false
        repeat {
            if couldEnd {
                self.advance()
                fenced = self.current == "/"
                continue
            }
            self.advance()
            couldEnd = self.current == "*"
        } while !self.isEOF && !fenced
        if fenced {
            self.closeTokenData()
            self.streamer.consume()
        } else {
            assumeErrorneous()
        }
    }
    
    /// Treat the forecoming caracters as being part of a *identifier* token.
    private mutating func assumeIdentifier() {
        repeat {
            self.advance()
        } while self.current ?= self.identifierName
        if let value = Bool(self.tokenData.buffer) {
            self.closeTokenData()
            self.register(token: .bool(self.tokenData, value))
            self.discard()
        } else {
            self.closeTokenData()
            self.register(token: .identifier(self.tokenData))
        }
    }
    
    private mutating func assumeNumeric(signed: Bool) {
        var mayBeDecimal = false
        var mayBeSemver = false
        var isInvalid = false
        let validChar = self.digits.union(CharacterSet(arrayLiteral: "."))
        let invalidPrevious = self.kind.union(CharacterSet(arrayLiteral: "."))
        repeat {
            self.advance()
            if self.current == "." {
                mayBeDecimal = !mayBeDecimal
                mayBeSemver = !mayBeDecimal && !signed
                isInvalid = self.previous ?= invalidPrevious || !mayBeDecimal && !mayBeSemver
            }
        } while !self.isEOF && !isInvalid && !mayBeSemver && self.current ?!= .whitespacesAndNewlines && self.current ?= validChar
        if isInvalid || self.previous == "." {
            self.assumeErrorneous()
        } else if mayBeSemver {
            self.assumeSemver()
        } else {
            self.closeTokenData()
            self.register(token: .number(self.tokenData, mayBeDecimal))
        }
    }
    
    private mutating func assumeSemver() {
        repeat {
            self.advance()
        } while !self.isEOF && self.current ?!= .whitespacesAndNewlines && self.current ?!= self.illegalInSemver
        self.closeTokenData()
        self.register(token: .semver(self.tokenData))
    }
    
    private mutating func assumeRevision() {
        repeat {
            self.advance()
        } while self.current ?= self.hex
        
        if self.current == ">" {
            self.advance()
            self.closeTokenData()
            self.register(token: .revision(self.tokenData))
        } else {
            self.assumeErrorneous()
        }
    }
    
    private mutating func assumeOperator() {
        self.advance()
        if self.current ?= "=~" {
            self.advance()
            self.closeTokenData()
            self.register(token: .op(self.tokenData))
        } else {
            self.assumeErrorneous()
        }
    }
    
    private mutating func assumeErrorneous() {
        repeat {
            self.advance()
        } while !self.isEOF && self.current ?!= .whitespacesAndNewlines && self.current ?!= "\""
        if self.current ?= "\"" {
            self.assumeString()
        } else {
            self.closeTokenData()
            self.register(token: .errorneous(self.tokenData))
        }
    }
}
