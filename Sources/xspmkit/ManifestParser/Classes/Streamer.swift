//
//  Streamer.swift
//  xspmkit
//

import Foundation
import ConvenientCharacterSets

class Streamer {
    struct Location: Equatable {
        private(set) var index: Int
        private(set) var line: Int
        private(set) var column: Int
        
        var position: ParserError.Position {
            return (self.column + 1, self.line + 1, self.index)
        }
        
        static let undefined: Location = .init(index: .min, line: .min, column: .min)
        
        fileprivate mutating func newLine() {
            self.line += 1
            self.column = 0
            self.index += 1
        }
        
        fileprivate mutating func advance() {
            self.column += 1
            self.index += 1
        }
    }
    
    private let str: String
    private var cursor: String.Index
    private var nextConsumeReturns: Bool = false
    private var isAtStart: Bool { return self.cursor == self.str.startIndex }
    
    var current: Character { return self.isEOF ? "\0" : str[cursor] }
    var previous: Character { return self.previousCursor().map{ self.str[$0] } ?? "\0" }
    var isEOL: Bool { return nextConsumeReturns  }
    var isEOF: Bool { return self.cursor >= self.str.endIndex }
    private(set) var location: Location
    private(set) var strippedWhiteSpace: Int
    
    
    init(text: String) {
        self.str = text
        self.cursor = self.str.startIndex
        self.location = Location(index: 0, line: 0, column: 0)
        self.strippedWhiteSpace = 0
    }
    
    func consume() {
        guard let next = self.nextCursor() else { return }
        self.cursor = next
        guard !self.isEOF else { return }
        if self.nextConsumeReturns {
            self.location.newLine()
            self.nextConsumeReturns.toggle()
        } else {
            self.location.advance()
        }
        switch self.current {
        case "\n":
            self.nextConsumeReturns.toggle()
        case "\r":
            let nextCursor = str.index(after: cursor)
            guard nextCursor < self.str.endIndex else { break }
            if self.str[nextCursor] == "\n" {
                self.consume()
            } else {
                self.nextConsumeReturns.toggle()
            }
        default:
            break
        }
    }
    
    func reset() {
        self.cursor = self.str.startIndex
        self.location = Location(index: 0, line: 0, column: 0)
        self.strippedWhiteSpace = 0
    }
    
    func stripWhiteSpace() {
        repeat {
            self.consume()
        } while self.current ?= .whitespaces
    }
    
    private func nextCursor() -> String.Index? {
        guard !self.isEOF else { return nil }
        return self.str.index(after: self.cursor)
    }
    
    private func previousCursor() -> String.Index? {
        guard !self.isAtStart else { return nil }
        return self.str.index(before: self.cursor)
    }
}
