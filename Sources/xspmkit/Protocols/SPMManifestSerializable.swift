//
//  SPMManifestSerializable.swift
//  xspmkit
//

protocol SPMManifestSerializable {
    var serialized: String { get }
}
