//
//  ComparableManifest.swift
//  xspmkit
//

import Foundation

protocol ComparableManifest {
    associatedtype Compared
    func compare(with rhs: Compared) throws -> ManifestComparisonResult
}
