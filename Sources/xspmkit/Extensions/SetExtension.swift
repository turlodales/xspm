//
//  SetExtension.swift
//  xspmkit
//
//  Created by Pierre TACCHI on 05/01/2019.
//

import Foundation

extension Set {
    var array: [Element] { return [Element](self) }
}
