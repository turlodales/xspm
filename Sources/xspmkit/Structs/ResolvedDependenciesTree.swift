//
//  ResolvedDependenciesTree.swift
//  xspmkit
//

import Foundation
import PathKit

// FIXME: We need better data structure in here.
public struct ResolvedDependenciesTree: Codable {
    fileprivate struct ResolvedDependency: Codable {
        let name: String
        let url: String
        let version: String
        let path: String
        let dependencies: [ResolvedDependency]
        
        func dependencyNames() -> [String] {
            var names = self.dependencies.map({ $0.dependencyNames() }).joined().wholeMap([String].init)
            names.append(self.name)
            
            return names.unique
        }
    }
    
    private let tree: [ResolvedDependency]
    private let productsPackage: [String: String]
    
    init(fromTreeData data: Data, buildPath: Path) throws {
        let decoder = JSONDecoder()
        let checkoutsPath = buildPath + "checkouts"
        self.tree = try decoder.decode(ResolvedDependency.self, from: data).dependencies
        let allPackages = Array(self.tree.map { $0.dependencyNames() }.joined()).unique
        self.productsPackage = try allPackages.reduce(into: [String: String]()) { (dict, package) in
            try ResolvedDependenciesTree.describe(package, from: checkoutsPath).targets.map { $0.name }.forEach { dict[$0] = package }
        }
    }
    
    public func dependencies(for package: String) -> [String] {
        return self.tree.first(where: { $0.name == package })?.dependencyNames() ?? []
    }
    
    public func version(for package: String) -> String {
        let version = self.tree.first(where: { $0.name == package })?.version ?? "1"
        return version == "unspecified" ? "1" : version
    }
    
    public func package(for product: String) -> String? {
        return self.productsPackage[product]
    }
    
    public func packageExists(_ package: String) -> Bool {
        return self.tree.map { $0.dependencyNames() }.flatMap { $0 }.set.contains(package)
    }
    
    private static func describe(_ packageName: String, from checkoutPath: Path) throws -> PackageDescription {
        let packagePath = checkoutPath + packageName
        
        let data = try SPM.perform(command: .describe, printOutput: false, saveSuccessLog: false, extraArgs: ["--type", "json"], inRoot: packagePath)
        guard let str = String(data: data, encoding: .utf8), let startIndex = str.firstIndex(of: "{"),
            let endIndex = str.lastIndex(of: "}"),let sanitizedData = String(str[startIndex...endIndex]).data(using: .utf8),
            let description = try? PackageDescription(fromTreeData: sanitizedData)
            else {
                throw XSPMError.cannotDecribePackage
        }
        
        return description
    }
}

extension ResolvedDependenciesTree.ResolvedDependency: CustomStringConvertible {
    var description: String {
        var deps = self.dependencies.map { "  \($0.description)".replacingOccurrences(of: "\n", with: "\n  ") }
        deps.insert(" ‣ \(self.name)", at: 0)
    
        return deps.joined(separator: "\n")
    }
}

extension ResolvedDependenciesTree: CustomStringConvertible {
    public var description: String {
        return self.tree.map { $0.description }.joined(separator: "\n")
    }
}
