//
//  ManifestContent.swift
//  xspmkit
//

import Foundation

public struct ManifestContent: Codable, Equatable {
    typealias PropertyKey = String
    
    public let targetProjectName: String
    public let embedDependenciesProject: Bool
    public let dependencyFolderName: String
    public let dependencies: [Dependency]
    public let dependenciesMappings: [DependencyMapping]
    public let toolsVersion: String?
    public let xcconfig: String?
    
    var dependenciesProducts: String {
        return dependencies.lazy.map { $0.products }.map { $0.joined(separator: ", ") }.joined(separator: ", ")
    }
    
    init(projectName: String, properties: [PropertyKey: Parser.Value], dependencies: [Dependency], mappings: [DependencyMapping]) {
        self.targetProjectName = projectName
        self.dependencies = dependencies
        self.dependenciesMappings = mappings
        self.dependencyFolderName = properties[.dependenciesName]?.asString ?? "Dependencies"
        self.embedDependenciesProject = properties[.embedDependencies]?.asBool ?? true
        self.toolsVersion = properties[.toolsVersion]?.asString
        self.xcconfig = properties[.xcconfig]?.asString
    }
}

extension ManifestContent: CustomStringConvertible {
    public var description: String {
        var output = String()
        output.appendLine("Target project: \(self.targetProjectName)")
        output.appendLine("Dependencies folder: \(self.dependencyFolderName)")
        output.lineFeed()
        self.dependencies.forEach {
            output.appendLine($0.description)
        }
        return output
    }
}

extension ManifestContent.PropertyKey {
    static let projectName: ManifestContent.PropertyKey = "project"
    static let dependenciesName: ManifestContent.PropertyKey = "dependencies"
    static let embedDependencies: ManifestContent.PropertyKey = "embed"
    static let toolsVersion: ManifestContent.PropertyKey = "toolsVersion"
    static let xcconfig: ManifestContent.PropertyKey = "xcconfig"
    
    static var allKeys: [ManifestContent.PropertyKey] {
        return [projectName, dependenciesName, embedDependencies, toolsVersion, xcconfig]
    }
    
    var isValid: Bool {
        return ManifestContent.PropertyKey.allKeys.contains(self)
    }
}

extension ManifestContent: ComparableManifest {
    typealias Compared = ManifestContent

    func compare(with rhs: ManifestContent) -> ManifestComparisonResult {
        LoggerManager.logDebug("Starting manifest content comparison.")
        var results: ManifestComparisonResult = []

        if self.targetProjectName != rhs.targetProjectName {
            LoggerManager.logDebug("Project name doesn't match.")
            return [.projectName]
        }
        if self.dependencyFolderName != rhs.dependencyFolderName {
            LoggerManager.logDebug("Dependencies names don't match.")
            results.formUnion(.dependenciesName)
        }
        if self.dependencies != rhs.dependencies {
            LoggerManager.logDebug("Dependencies don't match.")
            results.formUnion(.dependencies)
        }
        if self.dependenciesMappings != rhs.dependenciesMappings {
            LoggerManager.logDebug("Dependencies mapping don't match.")
            results.formUnion(.dependenciesMapping)
        }
        if self.embedDependenciesProject != rhs.embedDependenciesProject {
            LoggerManager.logDebug("Dependencies embedding flags don't match.")
            results.formUnion(.embedDependencies)
        }
        
        if self.toolsVersion != rhs.toolsVersion {
            LoggerManager.logDebug("Tools version don't match.")
            results.formUnion(.toolsVersion)
        }
        
        if self.xcconfig != rhs.xcconfig {
            LoggerManager.logDebug("Xcconfig don't match")
            results.formUnion(.xcconfig)
        }

        return results
    }
}
