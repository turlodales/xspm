#!/bin/sh
git clone https://gitlab.com/Pyroh/xspm.git
cd xspm
LAST_VERSION=`git describe --abbrev=0 --tags`
echo $LAST_VERSION
git checkout $LAST_VERSION
make install
cd ..
rm -rf xspm